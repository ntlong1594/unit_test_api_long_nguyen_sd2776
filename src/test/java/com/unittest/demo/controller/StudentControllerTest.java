package com.unittest.demo.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.unittest.demo.model.Student;
import com.unittest.demo.repository.StudentRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class StudentControllerTest {
	public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private StudentRepository studentRepositoryMock;

	@InjectMocks
	private StudentController studentController;

	@Before
	public void init() {
	}

	@Test
	public void findAll_StudentsFound_ShouldReturnFoundStudentEntries() throws Exception {
		Student first = new Student(1l, "Bob", "A1234567");
		Student second = new Student(2l, "Alice", "B1234568");

		when(studentRepositoryMock.findAll()).thenReturn(Arrays.asList(first, second));

		mockMvc.perform(get("/student")).andExpect(status().isOk())
				.andExpect(content().contentType(APPLICATION_JSON_UTF8)).andExpect(jsonPath("$", hasSize(2)))
				.andExpect(jsonPath("$[0].id", is(1))).andExpect(jsonPath("$[0].name", is("Bob")))
				.andExpect(jsonPath("$[0].passportNumber", is("A1234567"))).andExpect(jsonPath("$[1].id", is(2)))
				.andExpect(jsonPath("$[1].name", is("Alice")))
				.andExpect(jsonPath("$[1].passportNumber", is("B1234568")));

		verify(studentRepositoryMock, times(1)).findAll();
		verifyNoMoreInteractions(studentRepositoryMock);
	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Case GET STUDENT BY ID
	 * @throws Exception
	 */
	@Test
	public void retrieveStudentByID_StudentNotFound_ShouldThrowException() throws Exception {
		Student first = new Student(1l, "Bob", "A1234567");
		Optional<Student> opt = Optional.of(first);

		when(studentRepositoryMock.findById(1l)).thenReturn(opt);

		mockMvc.perform(get("/student/10000000")).andExpect(status().isNotFound());
	}

	@Test
	public void retrieveStudentByID_StudentFound_ShouldReturnStudentWithID() throws Exception {
		Student first = new Student(1l, "Bob", "A1234567");
		Optional<Student> opt = Optional.of(first);

		when(studentRepositoryMock.findById(1l)).thenReturn(opt);

		mockMvc.perform(get("/student/1")).andExpect(status().isOk())
				.andExpect(jsonPath("$.name", is("Bob"))).andExpect(jsonPath("$.passportNumber", is("A1234567")));
	}
	
	/**
	 * END Case GET STUDENT BY ID
	 * @throws Exception
	 */
	
	
	
	/**
	 * Case DELETE STUDENT BY ID
	 * @throws Exception
	 */
	@Test
	public void deleteStudentByIDTest() throws Exception {
		mockMvc.perform(delete("/student/1")).andExpect(status().isOk());
	}
	/**
	 * END Case DELETE STUDENT BY ID
	 * @throws Exception
	 */
	
	
	/**
	 * Case POST STUDENT
	 * @throws Exception
	 */
	@Test
	public void postStudentTest() throws Exception {
		Student student = new Student(1000L,"Long","123456789");
		
		String studentDTO = StudentControllerTest.asJsonString(student);
		when(studentRepositoryMock.save(any(Student.class))).thenReturn(student);

		mockMvc.perform(post("/student").contentType(MediaType.APPLICATION_JSON).content(studentDTO)).andExpect(status().isCreated());
	}
	/**
	 * END POST STUDENT
	 * @throws Exception
	 */
	
	
	/**
	 * Case PUT STUDENT
	 * @throws Exception
	 */
	@Test
	public void putStudentTest_CaseNotFound() throws Exception {
		Student student = new Student("Bob-new-update","99998887766");
		String studentDTO = StudentControllerTest.asJsonString(student);
		
		Student first = new Student(1l, "Bob", "A1234567");
		Optional<Student> opt = Optional.of(first);
		when(studentRepositoryMock.findById(1L)).thenReturn(opt);

		mockMvc.perform(put("/student/1000000").contentType(MediaType.APPLICATION_JSON).content(studentDTO)).andExpect(status().isNotFound());
	}
	
	@Test
	public void putStudentTest_ShouldReturnStudentUpdated() throws Exception {
		Student student = new Student("Bob-new-update","99998887766");
		String studentDTO = StudentControllerTest.asJsonString(student);
		
		Student first = new Student(1l, "Bob", "A1234567");
		Optional<Student> opt = Optional.of(first);
		when(studentRepositoryMock.findById(1L)).thenReturn(opt);

		mockMvc.perform(put("/student/1").contentType(MediaType.APPLICATION_JSON).content(studentDTO)).andExpect(status().isOk())
		.andExpect(jsonPath("$.name", is("Bob-new-update"))).andExpect(jsonPath("$.passportNumber", is("99998887766")));
	}
	/**
	 * END PUT STUDENT
	 * @throws Exception
	 */
	
	
	
}
